<?php
require 'random.php';

$data = $_POST['image'];

list($type, $data) = explode(';', $data);
list(, $data)      = explode(',', $data);
$data = base64_decode($data);

$ext = ".jpg";

if($type == "data:image/png"){
	$ext = "png";
}
else if($type == "data:image/jpeg"){
	$ext = "jpeg";
}
else if($type == "data:image/gif"){
	$ext = "gif";
}

$filename = time().'-'.rand_alpha_num(20).'.'.$ext;

file_put_contents('./uploads/'.$filename, $data);

if(file_exists('./uploads/'.$filename)){
	unlink('./uploads/'.$filename);
}

echo json_encode(['link' => $filename]);

?>
