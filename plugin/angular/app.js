var base_a = window.location.host;
var base_url = "http://"+base_a
/**
 * Binds a TinyMCE widget to <textarea> elements.
 */
angular.module('ui.tinymce', [])
    .value('uiTinymceConfig', {})
    .directive('uiTinymce', ['uiTinymceConfig', function(uiTinymceConfig) {
    uiTinymceConfig = uiTinymceConfig || {};
    var generatedIds = 0;
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ngModel) {
            var expression, options, tinyInstance;
            // generate an ID if not present
            if (!attrs.id) {
                attrs.$set('id', 'uiTinymce' + generatedIds++);
            }
            options = {
                // Update model when calling setContent (such as from the source editor popup)
                setup: function(ed) {
                    ed.on('init', function(args) {
                        ngModel.$render();
                    });
                    // Update model on button click
                    ed.on('ExecCommand', function(e) {
                        ed.save();
                        ngModel.$setViewValue(elm.val());
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                    });
                    // Update model on keypress
                    ed.on('KeyUp', function(e) {
                        console.log(ed.isDirty());
                        ed.save();
                        ngModel.$setViewValue(elm.val());
                        if (!scope.$$phase) {
                            scope.$apply();
                        }
                    });
                },
                mode: 'exact',
                elements: attrs.id
            };
            if (attrs.uiTinymce) {
                expression = scope.$eval(attrs.uiTinymce);
            } else {
                expression = {};
            }
            angular.extend(options, uiTinymceConfig, expression);
            setTimeout(function() {
                tinymce.init(options);
            });


            ngModel.$render = function() {
                if (!tinyInstance) {
                    tinyInstance = tinymce.get(attrs.id);
                }
                if (tinyInstance) {
                    tinyInstance.setContent(ngModel.$viewValue || '');
                }
            };
        }
    };
}]);

var app = angular.module("myApp", ['dndLists','ui.tinymce','ngSanitize']);

 app.directive('splitArray', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {

            function fromString(text) {
                return text.split("\n");
            }

            function toString(array) {
                return array.join("\n");
            }

            ngModel.$parsers.push(fromString);
            ngModel.$formatters.push(toString);
        }
    };
});
app.controller("myCtrl", function($scope, $http, $timeout) {

    $scope.generalOption = {
        "editable": true,
        "index": 1,
        "question": "",
        "description": "",
        "file": "",
        "fileObj": [],
        "links": ""
      };

      var option = [];
      option.section = collect($scope.generalOption,
          {
              component: "section",
              description: "Ini adalah question",
          });
      option.textDesc = collect($scope.generalOption,
          {
              component: "text",
              description: "<h1>Lorem ipsum dolor sit amet</h1>" + "<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>"

          });
      option.textBox = collect($scope.generalOption,
          {
              component: "textbox",
              description: "Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

          });

      option.divider = collect($scope.generalOption,
          {
              component: "divider",
          });

      option.image = collect($scope.generalOption,
        {
          component: "image",
          description: "Image",
          src: "builder/src/images/builder/image.png"

        });

      option.imageGroup = collect($scope.generalOption,
          {
              component: "imagegroup",
              description: "Static Image",

          });

      option.cards = collect($scope.generalOption,
          {
              component: "cards",
              description: "Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

          });

      option.socialFollow = collect($scope.generalOption,
          {
              component: "socialfollow",
              description: "Social Follow",
              options: ["https://instagram.com/","https://facebook.com/"],

          });
      option.button = collect($scope.generalOption,
          {
              component: "button",
              description: "Click Me",
              links: "http://"

          });

          option.footer = collect($scope.generalOption,
            {
              component: "footer",
              description: "Copyright | Your Company | All rights reserved.<br/><br/>Our mailing address is:<br/><br/>mail@kokkato.com<br/><br/>Want to change how you receive these emails?<br/><br/>You can update your preferences or unsubscribe from this list."

            });

        option.video = collect($scope.generalOption,
          {
            component: "video",
            description: "Masukan Video",
            links: ""

          });


      $scope.models = {
          selected: null,
          templates: [
              {
                  type: "section",
                  name: "Teks",
                  info: "Ini adalah teks",
                  img: "builder/src/images/builder/text.png",
                  options: option.section,
                  columns: [[]]
              },
              {
                  type: "text",
                  name: "Text",
                  info: "Ini adalah",
                  img: "builder/src/images/builder/text.png",
                  options: option.textDesc

              },
              {
                  type: "textbox",
                  name: "Text In Box",
                  info: "Ini adalah text in box",
                  img: "builder/src/images/builder/text-box.png",
                  options: option.textBox

              },
              {
                  type: "divider",
                  name: "Divider",
                  info: "Ini adalah divider.",
                  img: "builder/src/images/builder/divider.png",
                  options: option.divider

              },

              {
                  type: "image",
                  name: "Image",
                  info: "Ini adalah image.",
                  img: "builder/src/images/builder/image.png",
                  options: option.image

              },

              // {
              //     type: "imagegroup",
              //     name: "Image Grup",
              //     info: "Ini adalah image group.",
              //     img: "builder/src/images/builder/image-group.png",
              //     options: option.imageGroup
              //
              // },

              {
                  type: "cards",
                  name: "Card",
                  info: "Ini adalah card.",
                  img: "builder/src/images/builder/card.png",
                  options: option.cards

              },

              // {
              //     type: "socialfollow",
              //     name: "Social Follow",
              //     info: "Ini adalah social follow. ",
              //     img: "builder/src/images/builder/social-follow.png",
              //     options: option.socialFollow,
              //
              // },
              {
                  type: "button",
                  name: "Button",
                  info: "Ini adalah button",
                  img: "builder/src/images/builder/button.png",
                  options: option.button

              },

              {
                  type: "footer",
                  name: "Footer",
                  info: "Ini adalah footer.",
                  img: "builder/src/images/builder/footer.png",
                  options: option.footer

              },
              {
                  type: "video",
                  name: "Video",
                  img: "builder/src/images/builder/video.png",
                  info: "Ini adalah video.",
                  options: option.video

              }

          ],
          dropzones: { "A": [ {
              "options": option.section,
              "type": "section",
              "name": "Page",
              "columns": [ [  ] ]
              } ]
          }
      };

      $scope.updateSurveyRaw = function(){
        $scope.surveyRawData = {};
        $scope.surveyRawData.settings = $scope.settings;
        $scope.surveyRawData.formData = $scope.modelAsJson;
        $scope.surveyRawData = angular.toJson($scope.surveyRawData,true);

    }

      $scope.$ = $;

      // Model to JSON for demo purpose

      $scope.$watch('models.dropzones', function(model) {
        // $scope.modelAsJson = angular.toJson(model.A, true);
        $scope.modelAsJson = [];
        var survey_item_index = 0;
        angular.forEach(model.A, function (section, section_index) {
            section.options['index'] = survey_item_index;
            $scope.modelAsJson.push(section.options);
            survey_item_index++;
            angular.forEach(section.columns[0], function (item, item_index) {
                $scope.modelAsJson.push(item.options);
                survey_item_index++;
            });
        });
        $scope.updateSurveyRaw();

    }, true);

})

  app.directive("fileinput", ['$http', function($http) {
      return {
        scope: {
          fileinput: "=",
          filepreview: "="
        },
        link: function(scope, element, attributes) {
          element.bind("change", function(changeEvent) {
            scope.fileinput = changeEvent.target.files[0];
            var reader = new FileReader();
            reader.onload = function(loadEvent) {
              scope.$apply(function() {
                  //scope.filepreview = loadEvent.target.result;

                  $http({
                      method : "POST",
                      data: $.param({
                          "image" : loadEvent.target.result
                      }),
                      headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                      url : base_url+"/upload.php"
                  }).then(function(response) {
                      scope.filepreview = response.data.link;
                  });
              });
            }
            reader.readAsDataURL(scope.fileinput);
          });
        }
      }
    }]);
