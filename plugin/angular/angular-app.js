var base_url = $("#base_url").attr('href');
angular.module("app", ["dndLists","ngSanitize"]);
/**
 * The controller doesn't do much more than setting the initial data model
 */
 angular.module("app").directive('splitArray', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attr, ngModel) {

            function fromString(text) {
                return text.split("\n");
            }

            function toString(array) {
                return array.join("\n");
            }

            ngModel.$parsers.push(fromString);
            ngModel.$formatters.push(toString);
        }
    };
});

angular.module("app").controller("builderCtrl", function($scope, $http, $timeout) {

  $scope.generalOption = {
      "editable": true,
      "index": 1,
      "question": "Text input",
      "description": "",
      "placeholder": {
          "weight": 1
      },
      "options": [],
      "required": 1,
      "links": ""
    };

    var option = [];
    option.section = collect($scope.generalOption,
        {
            component: "section",
            question: "",
        });
    option.text = collect($scope.generalOption,
        {
            component: "text",
            question: "Ini adalah text",

        });
    option.textBox = collect($scope.generalOption,
        {
            component: "textbox",
            question: "Lorem IPSUM"

        });

    option.divider = collect($scope.generalOption,
        {
            component: "divider",
            question: "Divider",
        });

    option.image = collect($scope.generalOption,
      {
        component: "image",
        question: "Image",

      });

    option.imageGrup = collect($scope.generalOption,
        {
            component: "imagegrup",
            question: "Static Image",

        });

    option.card = collect($scope.generalOption,
        {
            component: "card",
            question: "Card"

        });

    option.socialFollow = collect($scope.generalOption,
        {
            component: "socialfollow",
            question: "Social Follow",
            options: ["https://instagram.com/","https://facebook.com/"],

        });
    option.button = collect($scope.generalOption,
        {
            component: "button",
            question: "Click Me",
            links: ""

        });

        option.footer = collect($scope.generalOption,
          {
            component: "footer",
            question: "Footer"

          });
          option.button = collect($scope.generalOption,
            {
              component: "video",
              question: "Masukan Video",
              links: ""

            });


    $scope.models = {
        selected: null,
        templates: [
            {
                type: "section",
                name: "Teks",
                info: "Ini adalah teks",
                img: "images/builder/text.png",
                options: option.section,
                columns: [[]]
            },
            {
                type: "text",
                name: "Text",
                info: "Ini adalah",
                img: "images/builder/text.png",
                options: option.textBox

            },
            {
                type: "textBox",
                name: "Text In Box",
                info: "Ini adalah text in box",
                img: "images/builder/text-box.png",
                options: option.textBox

            },
            {
                type: "divider",
                name: "Divider",
                info: "Ini adalah divider.",
                img: "images/builder/divider.png",
                options: option.divider

            },

            {
                type: "image",
                name: "Image",
                info: "Ini adalah image.",
                img: "images/builder/image.png",
                options: option.image

            },

            {
                type: "imageGroup",
                name: "Image Grup",
                info: "Ini adalah image group.",
                img: "images/builder/text-group.png",
                options: option.imageGroup

            },

            {
                type: "card",
                name: "Card",
                info: "Ini adalah card.",
                img: "images/builder/card.png",
                options: option.card

            },

            {
                type: "socialFollow",
                name: "Social Follow",
                info: "Ini adalah social follow. ",
                img: "images/builder/social-follow.png",
                options: option.socialFollow,

            },
            {
                type: "button",
                name: "Button",
                info: "This allows admin to provide more than one answer and allows surveyors to pick multiple answers from surveyee.",
                img: "images/builder/button.png",
                options: option.button

            },

            {
                type: "footer",
                name: "Footer",
                info: "Ini adalah footer.",
                img: "images/builder/footer.png",
                options: option.footer

            },
            {
                type: "video",
                name: "Video",
                info: "Ini adalah video.",
                options: option.video

            }

        ],
        dropzones: { "A": [ {
            "options": option.section,
            "type": "section",
            "name": "Page",
            "columns": [ [  ] ]
            } ]
        }
    };

    $scope.addItems = function (){
        var section = $scope.models['templates'][0];
        $scope.models['dropzones']['A'].push(section);
    }


})
