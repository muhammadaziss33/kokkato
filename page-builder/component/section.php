<script type="text/ng-template" id="section.html">
    <div class="section-element" style="background: #000;">
        <div class="column" dnd-list="list" ng-repeat="list in item.columns" ng-include="'lists.html'" dnd-allowed-types="['button','cards','divider','footer','imagegroup','image','socialfollow','textbox','text','video']">

        </div>
    </div>
</script>
