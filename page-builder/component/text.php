<script type="text/ng-template" id="text.html">
    <div class="k-paging">
      <div ng-bind-html="item.options.description"></div>
    </div>
    <a href="#modalText" class="btn btn-demo" ng-click="$('#modalText').modal('show')" data-toggle="modal" data-target="#modalText">
    <i class="fa fa-edit"></i> Edit</a>
    <a class="btn btn-demo" ng-click="list.splice($index, 1)">Delete</i></a>
    <div class="modal right fade" id="modalText" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document" style="width: 400px;">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel2">Text input</h4>
            </div>

            <div class="modal-body">
              <div class="form-group">
                <label>Input your description</label>
                <textarea data-ui-tinymce id="text" data-ng-model="item.options.description"></textarea>
              </div>
            </div>

          </div>
        </div>
      </div>

</script>
<style media="screen">
  .k-paging {
    width: 100%;
    max-width: 90%;
    margin-top: 30px;
    margin-bottom: 30px;
    padding: 0 20px;
  }
</style>

<script type="text/javascript">
tinymce.init({
    selector: "textarea#text",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
