<script type="text/ng-template" id="textbox.html">
    <!-- <input type="text" ng-model="item.options.question">
    <input type="text" ng-model="item.options.links"> -->

    <div class="k-parent-textbox">
      <div>
        <p ng-bind-html="item.options.description"></p>
      </div>
    </div>

    <a href="#modalTextBox" class="btn btn-demo" ng-click="$('#modalTextBox').modal('show')" data-toggle="modal" data-target="#modalText">
    <i class="fa fa-edit"></i> Edit</a>
    <a class="btn btn-demo" ng-click="list.splice($index, 1)">Delete</i></a>
    <div class="modal right fade" id="modalTextBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document" style="width: 400px;">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel2">Text input Box</h4>
            </div>

            <div class="modal-body">
              <div class="form-group">
                <label>Input your description</label>
                <textarea data-ui-tinymce id="texta" data-ng-model="item.options.description"></textarea>
              </div>
            </div>

          </div>
        </div>
      </div>
</script>

<style media="screen">
  .k-parent-textbox {
    width: 100%;
    max-width: 90%;
    margin:auto;
    border: 1px solid #f0f0f0;
    padding: 20px;
  }
</style>
