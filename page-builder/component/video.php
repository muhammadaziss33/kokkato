<script type="text/ng-template" id="video.html">
  {{item.options}}
    <div class="d-video" ng-hide="item.options.links == null">
      <iframe width="420" height="345" ng-src="{{item.options.links}}">
      </iframe>
    </div>

    <a href="#modalText" class="btn btn-demo" ng-click="$('#modalVideo').modal('show')" data-toggle="modal" data-target="#modalVideo">
    <i class="fa fa-edit"></i> Edit</a>
    <a class="btn btn-demo" ng-click="list.splice($index, 1)">Delete</i></a>
    <div class="modal right fade" id="modalVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
        <div class="modal-dialog" role="document" style="width: 400px;">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel2">Video</h4>
            </div>

            <div class="modal-body">
              <div class="form-group">
                <label>Input your url video (Youtube)</label>
                <input type="text" ng-model="item.options.links" class="form-control">
              </div>
              <button type="button" class="close btn btn-primary" data-dismiss="modal" aria-label="Close">Save</button>
            </div>

          </div>
        </div>
      </div>

</script>

<style media="screen">
  .d-video {
    width:100%; max-width: 90%; margin:auto;
  }
</style>
