<script type="text/ng-template" id="image.html">

            <div>
              <div class="k-parent-image">
                <img ng-hide="uploader.isHTML5" class="img-responsive">
              </div>

              <img ng-src="{{item.options.fileObj[$index]}}" class="option-img" style="width:150px;height:150px;"/>
              <div class="btn btn-default">
                      <i class="fa fa-picture-o"></i>
                      <input type="file" class="form-control" fileinput="file" filepreview="item.options.fileObj[$index]">
                    </div>
            </div>

            <a href="#modalImage" class="btn btn-demo" ng-click="$('#modalImage').modal('show')" data-toggle="modal" data-target="#modalImage">
            <i class="fa fa-edit"></i> Edit</a>
            <a class="btn btn-demo" ng-click="list.splice($index, 1)">Delete</i></a>
            <div class="modal right fade" id="modalImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                <div class="modal-dialog" role="document" style="width: 400px;">
                  <div class="modal-content">

                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title" id="myModalLabel2">Text input</h4>
                    </div>

                    <div class="modal-body">
                      <div class="form-group">
                        <label>upload your image</label>
                        <input type="file" nv-file-select="" uploader="uploader" multiple  /><br/>
                        <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                            <span class="glyphicon glyphicon-upload"></span> Upload
                        </button>
                        <button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
                            <span class="glyphicon glyphicon-trash"></span> Remove
                        </button>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
</script>

<style media="screen">
  .k-parent-image {
    width: 100%;
    max-width: 50%;
    margin:auto;
    border: 1px solid #f0f0f0;
    padding: 20px;
    background-image: url('src/builder/images/image.png');
    background-repeat: no-repeat;
    background-size: 100% 100%;
  }
</style>
