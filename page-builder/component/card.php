<script type="text/ng-template" id="cards.html">
  <div class="k-paging-card">
    <div class="k-head-card">
      <img src="{{item.options.src}}" class="img-responsive">
    </div>
    <div class="k-content">
      <div ng-repeat="items in uploader.queue">
        <img src="{{item.options.src}}" ng-hide="uploader.isHTML5" class="img-responsive">
        <div ng-show="uploader.isHTML5" class="img-responsive" ng-thumb="{ file: items._file, height: 100 }"></div>
      </div>
      <div ng-bind-html="item.options.description"></div>
    </div>
  </div>

  <a href="#modalText" class="btn btn-demo" ng-click="$('#modalCard').modal('show')" data-toggle="modal" data-target="#modalCard">
  <i class="fa fa-edit"></i> Edit</a>
  <a class="btn btn-demo" ng-click="list.splice($index, 1)">Delete</i></a>
  <div class="modal right fade" id="modalCard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
      <div class="modal-dialog" role="document" style="width: 400px;">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel2">Text input</h4>
          </div>

          <div class="modal-body">
            <div class="form-group">
              <label>Input your description</label>
              <input type="file" nv-file-select="" uploader="uploader" multiple  /><br/>
              <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                  <span class="glyphicon glyphicon-upload"></span> Upload
              </button>
              <button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
                  <span class="glyphicon glyphicon-trash"></span> Remove
              </button>

              <textarea data-ui-tinymce id="text" data-ng-model="item.options.description"></textarea>
            </div>
          </div>

        </div>
      </div>
    </div>

</script>

<style media="screen">
  .k-paging-card {
    width: 100%;
    max-width: 90%;
    margin-top: 30px;
    margin-bottom: 30px;
    padding: 20px;
    margin:auto;
  }
  .k-head-card {
    width: 100%;
    max-width: 50%;
    margin:auto;
    border: 1px solid #f0f0f0;
    padding: 20px;
    background-image: url('src/builder/images/image.png');
    background-repeat: no-repeat;
    background-size: 100% 100%;
  }

  .k-paging-card .k-content {
    margin-top: 30px;
    text-align: justify;
    color: #5d5d5d;
  }
</style>
