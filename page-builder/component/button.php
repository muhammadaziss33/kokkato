<script type="text/ng-template" id="button.html">
    <!-- <input type="text" ng-model="item.options.question">
    <input type="text" ng-model="item.options.links"> -->
    <div class="k-parent-button">
      <a class="k-btn k-btn-primary" href=="#">{{item.options.description}}</a>
    </div>
</script>

<style media="screen">
  .k-parent-button {
    width: 100%;
    max-width: 70%;
    margin: auto;
  }

  .k-btn {
    display: inline-block;
    padding: 6px 12px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
  }

  .k-btn-primary {
    color: #fff;
    background-color: #337ab7;
    border-color: #2e6da4;
    width: 100%;
    padding: 10px 0;
    font-size: 18px;
    margin-top: 30px; margin-bottom: 30px;
    font-weight: 400;
  }
</style>
