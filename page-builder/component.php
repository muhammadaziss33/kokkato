<script type="text/ng-template" id="lists.html">
  <div class="item selected" ng-repeat="item in list"
  dnd-draggable="item"
  dnd-effect-allowed="all"
  dnd-moved="list.splice($index, 1)"
  dnd-selected="models.selected = item"
  ng-class="{'selected': models.selected === item}"
  dnd-type="item.type"
  ng-include="item.type + '.html'">
</div>
</script>
<?php
foreach(glob(__DIR__. '/component/*.php') as $file){
  require_once($file);
}
?>
