<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="plugin/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="plugin/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/mobile.css">
<link rel="stylesheet" type="text/css" href="css/main.css">


<link rel="stylesheet" type="text/css" href="plugin/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css" href="plugin/datatables/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="plugin/datatables/css/dataTables.bootstrap.min.css">
<link rel="stylesheet" href="css/style2.css">

<body ng-app="myApp" ng-controller="myCtrl">

  <div id="sidebar">
		<div class="logo">
			<img src="images/logo.png" class="img-responsive" />
		</div>
		<div class="bar-menu">
			<ul class="main-menu">
				<li>
					<a href="dashboard.php" ><i class="fa fa-dashboard"></i> Overview </a>
					<a href="dashboard.php" class="menu-mbl active"><i class="fa fa-dashboard"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-phone"></i> Voice <span><i class="fa fa-angle-right"></i></span></a>
					<ul class="submenu">
						<li><a href="voice_upload.php">Upload / Blast</a></li>
						<li><a href="voice_report.php">Download Report</a></li>
						<li><a href="voice_blacklist.php">Add Blacklist</a></li>
						<li><a href="voice_create.php">Create New App</a></li>
						<li><a href="voice_app.php">List App</a></li>
					</ul>
					<a href="#" class="menu-mbl"><i class="fa fa-phone"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-comments"></i> SMS <span><i class="fa fa-angle-right"></i></span></a>
					<ul class="submenu">
						<li><a href="sms_upload.php">Upload / Blast</a></li>
						<li><a href="sms_report.php">Download Report</a></li>
						<li><a href="sms_blacklist.php">Add Blacklist</a></li>
						<li><a href="sms_create.php">Create New App</a></li>
						<li><a href="sms_app.php">List App</a></li>
					</ul>
					<a href="#" class="menu-mbl"><i class="fa fa-comments"></i></a>
				</li>
				<li>
					<a href="#" class="active"><i class="fa fa-envelope"></i> Email <span><i class="fa fa-angle-right"></i></span></a>
					<ul class="submenu">
						<li><a href="email_upload.php">Upload / Blast</a></li>
						<li><a href="email-upload-next.php">Create Template</a></li>
						<li><a href="email_report.php">Download Report</a></li>
						<li><a href="email_blacklist.php">Add Blacklist</a></li>
						<li><a href="email_create.php">Create New App</a></li>
						<li><a href="email_app.php">List App</a></li>
					</ul>
					<a href="#" class="menu-mbl"><i class="fa fa-envelope"></i></a>
				</li>
				<li>
					<a href="#"><i class="fa fa-users"></i> User Management <span><i class="fa fa-angle-right"></i></span></a>
					<ul class="submenu">
						<li><a href="user_list.php">User List</a></li>
						<li><a href="user_create.php">Create User</a></li>
						<li><a href="user_role.php">Role-Right Management</a></li>
						<li><a href="task_management.php">Task Management</a></li>
						<li><a href="user_edit_profile.php">Edit Company</a></li>
					</ul>
					<a href="#" class="menu-mbl"><i class="fa fa-envelope"></i></a>
				</li>
			</ul>
		</div>
	</div>
	<div id="mainbar">
		<div class="topbar">
			<form class="search-bar">
				<div class="input-group">
			      <input type="text" class="form-control" placeholder="Search for...">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
			      </span>
			    </div>
			</form>
			<div class="label-profile">
				Hamdadi
			</div>
			<ul class="top-menu">
				<li><a href="#"><i class="fa fa-home"></i></a></li>
				<li><a href="#"><i class="fa fa-user"></i></a></li>
				<li><a href="#"><i class="fa fa-question-circle"></i></a></li>
				<li><a href="#"><i class="fa fa-sign-out"></i></a></li>
			</ul>
			<div class="clearfix"></div>
		</div>
		<div class="title-bar">
			Email -  Create Template
			<div class="title-bar-left">

			</div>
		</div>
    <div class="container">
      <div class="row">
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#emailBuilder">Email Builder</a></li>
          <li><a data-toggle="tab" href="#preview">Preview</a></li>
        </ul>
      </div>
    </div>

    <div>
      <div class="container">
        <div class="row">
        <div class="col-md-8" style="margin-top:30px;">
          <div class="k-box-parents">
            <div class="tab-content">
              <div id="emailBuilder" class="tab-pane fade in active">
                <div class="survey-builder">
                  <div class="survey-builder-content">
                    <div class="survey-content" ng-include="'lists.html'" dnd-allowed-types="['section']" dnd-list="list" ng-repeat="(zone, list) in models.dropzones">
                    </div>
                  </div>
                </div>
              </div>
              <div id="preview" class="tab-pane fade in">
                <div class="survey-builder">
                  <div class="survey-builder-content">
                    <div data-ng-repeat="section in models.dropzones.A">
                      <div data-ng-repeat="item in section.columns[0] track by $index">
                        {{item}}
                        <div data-ng-show="item.type=='text'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='textbox'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='divider'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='image'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='cards'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='button'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='footer'">
                          {{item.options.description}}
                        </div>
                        <div data-ng-show="item.type=='video'">
                          {{item.options.description}}
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
      <textarea data-ng-model="surveyRawData" class="form-control" rows="50" style="display:none;"></textarea>
    </div>



        <div class="simpleDemo">
          <div class="col-md-4" style="margin-top:30px;">

            <div class="item k-builder-tools" data-ng-class="item.type=='section' ? 'k-section' : ''" data-ng-click="addItem()" ng-repeat="item in models.templates"
            dnd-draggable="item"
            dnd-effect-allowed="all"
            dnd-copied="item.id = item.id + 1"
            dnd-type="item.type"
            >
            <div class="k-box" data-ng-hide="item.info==null">
              <img src="{{item.img}}" class="img-responsive"/>
            </div>
            {{item.name}}
          </div>
        </div>
      </div>
      </div>
    </div>
	</div>
  </div><!-- container -->



<?php include 'page-builder/component.php';?>

<script type="text/javascript" src="plugin/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="plugin/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="plugin/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="plugin/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="plugin/bootstrap/js/general.js"></script>
<script src="plugin/angular/angular.min.js"></script>
<script src="plugin/angular/angular-sanitize.js"></script>
<script src="plugin/angular/angular-file-upload.min.js"></script>
<script src="plugin/angular/angular-dnd.min.js"></script>
<script src="plugin/angular/angular-message.min.js"></script>
<script src="plugin/angular/app.js"></script>


</body>
</html>
