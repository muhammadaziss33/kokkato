var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');

gulp.task('sass', function() {
  return gulp.src('scss/main.scss', { noCache: true, style: 'compressed' })
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('../css'))
});


gulp.task('watch', function(){
  gulp.watch('../css/**/*.css', ['sass']);
})

gulp.task('default',['sass','watch']);
